-module (funbox_qt_finder).
-export([
    start_link/1,
    search/2
]).

-behaviour(gen_server).
-export([
    init/1,
    handle_call/3,
    handle_cast/2,
    handle_info/2,
    terminate/2,
    code_change/3
]).

-include ("funbox_qt_search.hrl").

-record (state, {
    id = make_ref(),
    objects,
    index
}).

start_link(Objects) ->
    gen_server:start_link(?MODULE, Objects, []).

search(Pid, Conditions) ->
    rpc:async_call(node(), gen_server, call, [Pid, {search, Conditions}]).

init(Objects) ->
    ok = gen_server:cast(self(), {deffered_init, Objects}),
    State = #state{},
    {ok, State}.

handle_call({search, Conditions}, _From, State) ->
    Result = search(
       State#state.objects, 
       State#state.index,
       Conditions
    ),
    {reply, {ok, Result}, State};
handle_call(_Request, _From, State) ->
    {reply, {error, unknown_call}, State}.

handle_cast({deffered_init, ObjectsIn}, State) ->
    Objects = array:from_list(ObjectsIn),
    Index = build_index(Objects),
    {noreply, State#state{objects = Objects, index = Index}}.

handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.


build_index(Objects) ->
    Index = maps:fold(fun(Field, MaxValue, Acc) ->
        Idx = array:new([
            {size, MaxValue + 1},
            {fixed, true},
            {default, sets:new()}
        ]),
        maps:put(Field, Idx, Acc)
    end, #{}, ?FIELDS),
    array:foldl(fun(Id, Object, AccIn) ->
        lists:foldl(fun({Field, Value}, Acc) ->
            Idx = maps:get(Field, Acc),
            Set = array:get(Value, Idx),
            NewSet = sets:add_element(Id, Set),
            NewIdx = array:set(Value, NewSet, Idx),
            maps:update(Field, NewIdx, Acc)
        end, AccIn, Object)
    end, Index, Objects).

search(Objects, Index, Conditions) ->
    ResultSet = sets:intersection(lists:map(fun({Field, From, To}) ->
        Idx = maps:get(Field, Index),
        sets:union(lists:map(fun(Value) ->
            array:get(Value, Idx)
        end, lists:seq(From, To)))
    end, Conditions)),
    sets:fold(fun(Id, Result) ->
        Object = array:get(Id, Objects),
        [Object | Result]
    end, [], ResultSet).
