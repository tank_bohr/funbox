-module (funbox_qt_search).

-export([start/0]).
-compile([export_all]).

-define (COUNT, 10000000).
-include ("funbox_qt_search.hrl").

start() ->
    io:format("Generating objects...~n"),
    {Time1, Objects} = timer:tc(?MODULE, generate_objects, []),
    io:format("~p microseconds elapsed~n", [Time1]),
    Shards = make_shards(Objects),
    Finders = lists:map(fun(Shard) ->
        {ok, Pid} = funbox_qt_finder:start_link(Shard), Pid
    end, Shards),
    Conditions = [
        {gender, 0, 0},
        {age, 17, 22},
        {height, 170, 185}
    ],
    Result = search(Finders, Conditions),
    io:format("~p~n", [Result]).

generate_objects() ->
    lists:map(fun(_I) ->
        lists:map(fun(Field) ->
            MaxValue = maps:get(Field, ?FIELDS),
            Value = generate_random(MaxValue),
            { Field, Value }
        end, maps:keys(?FIELDS))
    end, lists:seq(1, ?COUNT)).

generate_random(N) ->
    random:uniform(N + 1) - 1.

make_shards(Objects) ->
    make_shards(Objects, 1000).

make_shards(Objects, N) ->
    make_shards(Objects, N, []).

make_shards([], _, Acc) ->
    Acc;
make_shards(Objects, N, Acc) ->
    {Shard, Rest} = lists:split(N, Objects),
    make_shards(Rest, N, [Shard | Acc]).

search(Finders, Conditions) ->
    AsyncKeys = lists:map(fun (Finder) ->
        funbox_qt_finder:search(Finder, Conditions)
    end, Finders),
    lists:flatten(lists:map(fun (AsyncKey) ->
        case rpc:nb_yield(AsyncKey, 3000) of
            {value, {ok, Result}} ->
                Result;
            timeout ->
                io:format("Timeout ~n"),
                []
        end
    end, AsyncKeys)).
