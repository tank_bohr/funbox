#!/usr/bin/env ruby

require_relative "./grabber.rb"

(site, dest) = ARGV
Grabber.new(site, dest).grab
