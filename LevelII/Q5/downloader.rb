require "net/http"
require "nokogiri"

class Downloader
  attr_reader :page, :host

  def initialize(host, page = nil)
    @host = host
    set_page(page) if page
  end

  def set_page(page)
    @page = Nokogiri::HTML(page)
  end

  def download_images(dest)
    partitions(pictures).map do |part|
      thread = spawn_thread(part, dest)
      thread.abort_on_exception = true
      thread
    end.map(&:join)
  end

  def download_image(uri, dest)
    uri = make_uri(uri)
    Net::HTTP.start(uri.host) do |http|
      path = uri.path
      file = File.join(dest, File.basename(path))
      File.open(file, 'wb') do |f|
        http.get(path) do |segment|
          f.write(segment)
        end
      end
    end
  end
  protected :download_image

  private

  def pictures
    page.css('img').to_a.map do |elem|
      src_attr = elem.attribute("src")
      img_src = src_attr ? src_attr.value : nil
    end.compact.delete_if(&:empty?)
  end

  def make_uri(path)
    uri = URI(path)
    unless uri.host
      uri.scheme = 'http'
      uri.host = host
    end
    return uri
  end

  def partitions(list)
    list.each_with_index.group_by do |item, index|
      index % 4
    end.each_value.map do |partition|
      partition.map { |item, _index| item }
    end
  end

  def spawn_thread(part, dest)
    Thread.new do
      part.each do |uri|
        self.download_image(uri, dest)
      end
    end
  end
end
