require 'spec_helper'

describe Grabber do
  describe "#grab" do

    before(:each) do
      stub_request(:get, "http://www.example.com/")\
        .to_return(:status => 200, :body => "<stubbed_page>")
    end

    it "grabs" do
      downloader = double()
      grabber = described_class.new("www.example.com", "/tmp", downloader)
      expect(downloader).to receive(:set_page).with("<stubbed_page>")
      expect(downloader).to receive(:download_images).with("/tmp")
      grabber.grab()
    end
  end
end