require 'spec_helper'

describe Downloader do
  let(:host) { "www.example.com" }
  let(:page) do
    _ = <<HTML
<html>
  <body>
    <img src="/images/pic1.png" />
    <img src="/images/pic2.png" />
    <img src="/images/pic3.png" />
    <img src="/images/pic4.png" />
  </body>
</html>
HTML
  end

  describe "#set_page" do

    subject {described_class.new(host)}

    it "sets page" do
      subject.set_page(page)
      expect(subject.page).to be_kind_of(Nokogiri::HTML::Document)
    end
  end

  describe "#download_images" do

    subject {described_class.new(host, page)}

    it "downloads images" do
      expect(subject).to receive(:download_image).with("/images/pic1.png", "/tmp")
      expect(subject).to receive(:download_image).with("/images/pic2.png", "/tmp")
      expect(subject).to receive(:download_image).with("/images/pic3.png", "/tmp")
      expect(subject).to receive(:download_image).with("/images/pic4.png", "/tmp")
      subject.download_images("/tmp")
    end
  end

end
