require 'webmock/rspec'

require_relative '../grabber.rb'
require_relative '../downloader.rb'

RSpec.configure do |config|
  config.expect_with(:rspec) {|c| c.syntax = :expect}
  config.order = :random
end
