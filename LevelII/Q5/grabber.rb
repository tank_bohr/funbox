require "net/http"
require_relative "./downloader.rb"

class Grabber
  attr_reader :uri, :dest, :downloader

  def initialize(site, dest, downloader = nil)
    site = canonize_url(site)
    @uri = URI(site)
    @dest = dest
    @downloader = downloader ? downloader\
      : Downloader.new(uri.host)
  end

  def grab()
    page = download_page()
    downloader.set_page(page)
    downloader.download_images(dest)
  end

  private

  def download_page
    Net::HTTP.get(uri)
  end

  def canonize_url(url)
    url =~ /^https?\:\/\// ? url : "http://#{url}"
  end
end
